#include "CBarriereImpact.h"

bool CBarriereSympact::Connexion(QString IPadress)
{
    maSocket.connectToHost(IPadress,502);

    if(!maSocket.waitForConnected())
     {
        qDebug() << "Error Connection";
         return false;
     }

    return true;
}

void CBarriereSympact::Activation()
{
    CBarriereSympact::ecrireCMD(0x00,0x06);
    CBarriereSympact::ecrireCMD(0x00,0x07);
}

void  CBarriereSympact::ecrireCMD(unsigned short fort, unsigned short faible)
{
    TAB[0] = 0;
    TAB[1] = 0;
    TAB[2] = 0;
    TAB[3] = 0;
    TAB[4] = 0;
    TAB[5] = 0x06;
    TAB[6] = 0x01;
    TAB[7] = 0x06;
    TAB[8] = 0x21;
    TAB[9] = 0x35;
    TAB[10] = fort;
    TAB[11] = faible;

    maSocket.write(TAB);
    maSocket.waitForBytesWritten();
    maSocket.waitForReadyRead();
    qDebug() << maSocket.readAll();
}

void CBarriereSympact::Deconnexion()
{
    maSocket.disconnectFromHost();
}

void CBarriereSympact::Desactivation()
{
   CBarriereSympact::ecrireCMD(0x00,0x00);
}

void CBarriereSympact::Descente()
{
    CBarriereSympact::ecrireCMD(0x08,0x0f);
}

void CBarriereSympact::Monter()
{
   CBarriereSympact::ecrireCMD(0x00,0x0f);
}

int CBarriereSympact::lectureEtatBarriere()
{

    maSocket.write(TABetat);
    maSocket.waitForBytesWritten();
    maSocket.waitForReadyRead();
    etat = maSocket.readAll();

    return etat[10];
}

CBarriereSympact::CBarriereSympact()
{
    TAB.resize(12);
    TABetat.resize(12);

    TABetat[0] = 0;
    TABetat[1] = 0;
    TABetat[2] = 0;
    TABetat[3] = 0;
    TABetat[4] = 0;
    TABetat[5] = 0x06;
    TABetat[6] = 0x01;
    TABetat[7] = 0x03;
    TABetat[8] = 0x0c;
    TABetat[9] = 0x81;
    TABetat[10] = 0x00;
    TABetat[11] = 0x01;
}
