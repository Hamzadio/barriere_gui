#ifndef CBARRIEREIMPACT_H
#define CBARRIEREIMPACT_H

#include <chrono>   // pour std::this_thread::sleep_for(3000ms);
#include <thread>   // car les QThread bloquent l'application !
#include <QTcpSocket>
#include <QString>

using namespace std;

class CBarriereSympact
{

private:
    int etatVariateur;

    void ecrireCMD(unsigned short fort, unsigned short faible);

    QTcpSocket maSocket;

    QByteArray TAB;
    QByteArray TABetat;
    QByteArray etat;



public:

    bool Connexion(QString IPadress);


    void Activation();
    void Deconnexion();
    void Desactivation();
    void Descente();
    void Monter();

    void signalConnectState();
    void signalSocketState();
    void signalStateVariateur();


    int lectureEtatBarriere();


    CBarriereSympact();


};


























#endif // CBARRIEREIMPACT_H
