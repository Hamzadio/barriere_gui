#include <CBarriereImpact.h>

int main()
{
    using namespace std::chrono_literals; // pour indiquer le temps d'attente en ms

    bool valRet;
    CBarriereSympact maBarriere;

    //[1] Connection a la barriere
    qDebug() << "[1] Connection Barriere : ";
    valRet = maBarriere.Connexion("127.0.0.1");
    qDebug() << "[1] Retour Connection Barriere : "  << valRet;

    if (!valRet) { qDebug() << "Pb de connection avec la barriere ";
        return -1;     // on arrete, il n'y a pas de connexion
    }
    else
    {

        qDebug() << "[2] Mise en fonctionnement de la Barriere ";
        //maBarriere.Activation();


        qDebug() << "[3] Demande de Descente de la Barriere ";
        qDebug() << Qt::hex << static_cast<int> (maBarriere.lectureEtatBarriere());
       // maBarriere.Descente();

        qDebug() << "[3] Attente de 3 secondes";
        std::this_thread::sleep_for(6000ms);


        qDebug() << "[4] Demande de Monter de la Barriere (Attente etat 0x27)";
        //maBarriere.Monter();
        //qDebug() << maBarriere.lectureEtatBarriere();

        qDebug() << "[4] Attente de 3 secondes";
        std::this_thread::sleep_for(5000ms);

        //maBarriere.Desactivation();
        //[5] Desactivation de la Barriere : "
        qDebug() << "[5] Demande de Desactivation de la Barriere ! (Attente etat 0x40)";


        //[6] Fermeture de la socket (Deconnection Barriere)
        qDebug() << "[6] Demande de fermeture de la socket";

        maBarriere.Deconnexion();

    }

    return 0;
}
